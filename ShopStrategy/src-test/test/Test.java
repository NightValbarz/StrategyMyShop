package test;

import java.util.Arrays;

import framework.Concrete.Cart;
import framework.Concrete.ProductUnitarian;
import framework.Enum.Day;
import framework.Enum.ProductType;

class Test {

	Cart cart = new Cart(Day.MONDAY);

	@org.junit.jupiter.api.Test
	void testAddingProducts() {
		cart.addProductToCart(new ProductUnitarian("Leche", 15.50, ProductType.LACTEOS, "lt"), 10.0);
		cart.addProductToCart(new ProductUnitarian("Crema", 10.0, ProductType.LACTEOS, "kg"), 0.5);
		cart.addProductToCart(new ProductUnitarian("Mantequilla", 4.50, ProductType.LACTEOS, "kg"), 0.250);
		cart.addProductToCart(new ProductUnitarian("Queso", 100.0, ProductType.LACTEOS, "kg"), 0.5);
		cart.addProductToCart(new ProductUnitarian("Platano", 14.0, ProductType.FRUTA, "kg"), 1.0);
		cart.addProductToCart(new ProductUnitarian("Manzana", 13.0, ProductType.FRUTA, "kg"), 2.0);
	}

	@org.junit.jupiter.api.Test
	void testSalesMondayElder() {

		cart.setElder(true);
		cart.purchase();
	}

	@org.junit.jupiter.api.Test
	void testAlldaysNonElder() {

		cart.addProductToCart(new ProductUnitarian("Leche", 15.50, ProductType.LACTEOS, "lt"), 10.0);
		cart.addProductToCart(new ProductUnitarian("Crema", 10.0, ProductType.LACTEOS, "kg"), 0.5);
		cart.addProductToCart(new ProductUnitarian("Mantequilla", 4.50, ProductType.LACTEOS, "kg"), 0.250);
		cart.addProductToCart(new ProductUnitarian("Queso", 100.0, ProductType.LACTEOS, "kg"), 0.5);
		cart.addProductToCart(new ProductUnitarian("Platano", 14.0, ProductType.FRUTA, "kg"), 1.0);
		cart.addProductToCart(new ProductUnitarian("Manzana", 13.0, ProductType.FRUTA, "kg"), 2.0);

		Arrays.stream(Day.values()).forEach(day -> {
			System.out.println("\n" + day + " \n");
			cart.changeDay(day);
			cart.purchase();
		});
	}

}
