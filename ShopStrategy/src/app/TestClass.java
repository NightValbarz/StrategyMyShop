package app;

import framework.Concrete.Cart;
import framework.Concrete.ProductUnitarian;
import framework.Enum.Day;
import framework.Enum.ProductType;

public class TestClass {

	public static void main(String[] args) {
		Cart cart = new Cart(Day.MONDAY);
		
		cart.addProductToCart(new ProductUnitarian("Leche", 15.50, ProductType.LACTEOS, "lt"), 10.0);
		cart.addProductToCart(new ProductUnitarian("Crema", 10.0, ProductType.LACTEOS, "kg"), 0.5);
		cart.addProductToCart(new ProductUnitarian("Mantequilla", 4.50, ProductType.LACTEOS, "kg"), 0.250);
		cart.addProductToCart(new ProductUnitarian("Queso", 100.0, ProductType.LACTEOS, "kg"), 0.5);
		cart.addProductToCart(new ProductUnitarian("Platano", 14.0, ProductType.FRUTA, "kg"), 1.0);
		cart.addProductToCart(new ProductUnitarian("Manzana", 13.0, ProductType.FRUTA, "kg"), 2.0);
		
		cart.purchase();
	}

}
