package framework.Concrete;

import java.util.ArrayList;
import java.util.Arrays;

import framework.Abstract.AbstractMultiPromotion;
import framework.Enum.ProductType;

public class PromotionDairyAndSausages extends AbstractMultiPromotion {

	public PromotionDairyAndSausages() {
		super.types = new ArrayList<>(Arrays.asList(ProductType.EMBUTIDOS, ProductType.LACTEOS));
	}
	
	public String toString() {
		return "Promocion Lacteos y Embutidos!";
	}
}
