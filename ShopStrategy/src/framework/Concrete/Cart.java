package framework.Concrete;

import java.util.Comparator;
import java.util.TreeMap;

import framework.Enum.Day;
import framework.Interface.Product;

public class Cart {

	Day day;
	private Boolean elder = false;
	private Sales sales;

	private TreeMap<Product, Double> productlist = new TreeMap<>(Comparator.comparing(Product::toString));

	public Cart(Day day) {
		this.day = day;
	}

	public void addProductToCart(Product product, Double quantity) {
		productlist.put(product, quantity);
	}
	
	public void removeProduct(Product toremove) {
		this.productlist.remove(toremove);
	}

	public void purchase() {
		switch (day) {
		
		case MONDAY:
			if (elder)
				sales = new Sales(new PromotionElders());
			else
				sales = new Sales(null);
			break;
		case TUESDAY:
			sales = new Sales(null);
			break;
		case WEDNESDAY:
			sales = new Sales(new PromotionDairy());
			break;
		case THURSDAY:
			sales = new Sales(new PromotionFruits());
			break;
		case FRIDAY:
			sales = new Sales(new PromotionDairyAndSausages());
			break;
		case SATURDAY:
			sales = new Sales(null);
			break;
		case SUNDAY:
			if (elder)
				sales = new Sales(new PromotionElders());
			else 
				sales = new Sales(null);
			break;
		default:
			sales = new Sales(null);
			break;
		}

		sales.setProducts(productlist);

		System.out.println(sales.toString());
	}

	public void setElder(Boolean elder) {
		this.elder = elder;
	}
	
	public void changeDay(Day day) {
		this.day = day;
	}
	
	public void printProducts() {
		System.out.println(this.productlist);
	}

}
