package framework.Concrete;

import framework.Abstract.AbstractProductUnitarian;
import framework.Enum.ProductType;

public class ProductUnitarian extends AbstractProductUnitarian{

	public ProductUnitarian(String name, Double price, ProductType type, String unit) {
		super(name, price, type, unit);
	}
}
