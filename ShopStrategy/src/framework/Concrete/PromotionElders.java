package framework.Concrete;

import java.util.ArrayList;
import java.util.Arrays;

import framework.Abstract.AbstractMultiPromotion;
import framework.Enum.ProductType;

public class PromotionElders extends AbstractMultiPromotion {

	public PromotionElders() {
		super.types = new ArrayList<>(
				Arrays.asList(ProductType.EMBUTIDOS, ProductType.FRUTA, ProductType.LACTEOS, ProductType.VERDURA));
	}
	
	public String toString() {
		return "Promocion tercera edad!";
	}
}
