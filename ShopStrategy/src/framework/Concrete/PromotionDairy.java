package framework.Concrete;

import framework.Abstract.AbstractSimplePromotion;
import framework.Enum.ProductType;

public class PromotionDairy extends AbstractSimplePromotion {

	public PromotionDairy() {
		super.type = ProductType.LACTEOS;
	}
	
	public String toString() {
		return "Promocion Lacteos!";
	}
}
