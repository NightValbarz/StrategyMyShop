package framework.Concrete;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import framework.Interface.Product;
import framework.Interface.Promotion;

public class Sales {

	private final Double IVA = 1.16;
	private Double subtotal = 0.0;

	private TreeMap<Product, Double> productlist = new TreeMap<>(Comparator.comparing(Product::toString));
	private List<String> ticketgen = new ArrayList<>();
	Promotion promo;

	public Sales(Promotion promo) {
		this.promo = promo;
	}

	public void setProducts(TreeMap<Product, Double> productlist) {
		this.productlist = productlist;
		calculate();
	}

	private void calculate() {

		subtotal = 0.0;

		for (Map.Entry<Product, Double> entry : this.productlist.entrySet()) {

			ticketgen.add(entry.getKey().printFormat(entry.getValue()));

			if (promo != null && promo.doesPromoApply(entry.getKey().getType())) {
				subtotal = subtotal + (entry.getKey().getPrice() * entry.getValue()) * promo.getDiscount();
				ticketgen.add("discount applied! "
						+ String.valueOf((entry.getKey().getPrice() * entry.getValue()) * promo.getDiscount()));
			}

			if (promo != null && !promo.doesPromoApply(entry.getKey().getType())) {
				subtotal = subtotal + (entry.getKey().getPrice() * entry.getValue());
				ticketgen.add(String.valueOf((entry.getKey().getPrice() * entry.getValue())));
			}
			if (promo == null) {
				subtotal = subtotal + (entry.getKey().getPrice() * entry.getValue());
				ticketgen.add("---------------" + String.valueOf((entry.getKey().getPrice() * entry.getValue())));
			}

		}
	}

	public String toString() {
		StringBuilder builder = new StringBuilder("");
		builder.append("Ticket de Compra!\n");
		ticketgen.forEach(line -> builder.append(line + "\n"));
		builder.append("---------------" + subtotal.toString() + "\n");
		builder.append("---------------16 % IVA\n");
		if (promo != null)
			builder.append(promo + "\n");
		builder.append("Total a pagar->$" + subtotal * IVA);
		return builder.toString();
	}

	public Double getTotal() {
		return this.subtotal;
	}

}
