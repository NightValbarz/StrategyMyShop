package framework.Concrete;

import framework.Abstract.AbstractSimplePromotion;
import framework.Enum.ProductType;

public class PromotionFruits extends AbstractSimplePromotion {

	public PromotionFruits() {
		super.type = ProductType.FRUTA;
	}
	
	public String toString() {
		return "Promocion Frutas!";
	}
}
