package framework.Interface;

import framework.Enum.ProductType;

public interface Product {
	
	public void setName(String name);
	public String getName();
	public void setPrice(Double price);
	public Double getPrice();
	public ProductType getType();
	public void changeType(ProductType type);
	public String printFormat(Double quantity);
}
