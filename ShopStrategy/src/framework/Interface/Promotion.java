package framework.Interface;

import framework.Enum.ProductType;

public interface Promotion {

	public Double getDiscount();
	public Boolean doesPromoApply(ProductType type);

}
