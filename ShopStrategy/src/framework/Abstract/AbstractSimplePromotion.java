package framework.Abstract;

import framework.Enum.ProductType;
import framework.Interface.Promotion;

public abstract class AbstractSimplePromotion implements Promotion {

	protected ProductType type;
	protected Double discount = 0.90;
	
	@Override
	public Boolean doesPromoApply(ProductType type) {
		return this.type == type;
	}
	
	public void changeDiscount(Double newdiscount) {
		this.discount = newdiscount;
	}
	
	@Override
	public Double getDiscount() {
		return discount;
	}
}
