package framework.Abstract;

import framework.Enum.ProductType;
import framework.Interface.Product;

public abstract class AbstractProduct implements Product{

	protected Double price;
	protected String name;
	protected ProductType type;
	
	public AbstractProduct(String name, Double price, ProductType type) {
		this.price = price;
		this.type = type;
		this.name = name;
	}
	
	public void setPrice(Double newprice) {
		this.price = newprice;
	}
	
	public void setName(String newname) {
		this.name = newname;
	}
	
	public Double getPrice() {
		return this.price;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String toString() {
		return this.name +  " $ "   +this.price;
	}
	
	public ProductType getType() {
		return this.type;
	}
	
	public String printFormat(Double quantity) {
		return this.name +"   "+ quantity.toString() + "   " + this.price.toString();
	}
	
	public void changeType(ProductType type) {
		this.type = type;
	}
}
