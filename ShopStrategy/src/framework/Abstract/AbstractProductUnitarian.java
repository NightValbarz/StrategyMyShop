package framework.Abstract;

import framework.Enum.ProductType;

public abstract class AbstractProductUnitarian extends AbstractProduct {

	private String unit;
	
	public AbstractProductUnitarian(String name, Double price, ProductType type, String unit) {
		super(name, price, type);
		this.unit = unit;
	}
	
	public String printFormat(Double quantity) {
		return super.name +"   "+ quantity.toString()+ this.unit + "   " + super.price.toString();
	}
	
	public String getUnit() {
		return this.unit;
	}
	
	public void changeUnit(String unit) {
		this.unit = unit;
	}

}
