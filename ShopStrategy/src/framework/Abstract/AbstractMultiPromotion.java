package framework.Abstract;

import java.util.List;

import framework.Enum.ProductType;
import framework.Interface.Promotion;

public abstract class AbstractMultiPromotion implements Promotion{

	protected List<ProductType> types;
	protected Double discount = 0.95;
	
	@Override
	public Boolean doesPromoApply(ProductType type) {
		return types.contains(type);
	}
	
	public void changeDiscount(Double newdiscount) {
		this.discount = newdiscount;
	}
	
	@Override
	public Double getDiscount() {
		return discount;
	}
}
